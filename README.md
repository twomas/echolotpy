# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Python software for controlling a Kapsch TRX-1320

### How do I get set up? ###

* Local eth ip shall be 192.168.0.254
* Please use command close when to terminate application due to more stable communication with trx
* Tested with Windows 10 and Python 2.7.13
* Tested with Raspberry Pi 3 and Raspian Stretch (Use start script prepare-start.sh to prepare local eth ip, see rpi3-prepare.txt for more info)

Options:  
>help  
 01 help - Display options  
 02 close - Close application  
 03 debugoff - Disable debug logs  
 04 debugon - Enable debug logs  
 05 listparams - Display all parameters  
 06 listsubch - Display how to change subcarrier frequency  
 07 listch - Display how to change carrier frequency  
 08 setpower - Set power, ex: setpower 28  
 09 setechoes - Set number of echoes, ex: setechoes 10  
 10 setechosize - Set number extra data bytes for echo, ex: setechosize 1  
 11 setreftime - Set reflect time in seconds, ex: setreftime 60  
 12 echo - Start one echo  
 13 echoloop - Toggle enable or disable echo loop mode  
 14 x - Disable echo loop mode  
 15 reflect - Start reflect mode  
 16 color - Toggle enable or disable std color  

 ### Who do I talk to? ###

* thomas.bredhammar@kapsch.net