#!/usr/bin/env python
from __future__ import division # https://stackoverflow.com/questions/1267869/how-can-i-force-division-to-be-floating-point-division-keeps-rounding-down-to-0
import sys, socket, threading, os, time, struct, math, json

Defaultpower = 10
Defaultechoes = 10
Defaultechoextrasize = 0
Defaultreflecttime = 60
Defaultchannel = 2
Defaultsubchannel = 1
Defaultstdcolor = False

Configlocalip = '0.0.0.0'
Configlocalport = '5000'
Configlocaluserip = '0.0.0.0'
Configlocaluserport = '5001'

class UserStdThread(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.receiver = None
        self.infile = sys.stdin
        print('[+] New user thread started for std')

    def setreceiver(self, receiver):
        self.receiver = receiver
       
    def run(self):
        while True:
            infile=sys.stdin
            line=' '
            while len(line)!=0:
                line=infile.readline()
                command = line.strip()
                try:
                    if self.receiver:
                        self.receiver.command(command)
                    else:
                        if command == 'close':
                            os._exit(1)
                except:
                    pass
                
class PacketReadlineDecoder(object):

    def __init__(self):
        self._stream = ''

    def feed(self, buf):
        self._stream += buf

    def decode(self):
        '''
        Yields packets from the current stream.
        ''' 
        while len(self._stream) > 0:
            stripped = lambda s: "".join(i for i in s if 9 < ord(i) < 127)
            self._stream = stripped(self._stream)
            
            found = self._stream.find('\r\n')
            if found < 0:
                break
            
            text = self._stream.split('\r\n')
            packet = text[0]

            end = found + len('\r\n')
            packet = self._stream[:end]
            yield packet
            self._stream = self._stream[end:]
                
class UserSocketThread(threading.Thread):

    def __init__(self, ip, port):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.csocket = None
        self.dorun = True
        self.receiver = None
        self.sender = None
        print('[+] New user thread started for '+self.ip+':'+str(self.port))
        
    def close(self):
        self.dorun = False
        if self.csocket:
            self.csocket.close()
            
    def setreceiver(self, receiver):
        self.receiver = receiver
            
    def output(self, message):
        if self.csocket:
            self.csocket.send(message + '\r\n')
            
    def seterror(self, level):
        self.output('errorlevel:'+str(level))

    def run(self):
        data = ' '
        tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        tcpsock.bind((self.ip, int(self.port)))
        
        decoder = PacketReadlineDecoder()
         
        while self.dorun:
            tcpsock.listen(0)
            try:
                (self.csocket, (ip, port)) = tcpsock.accept()
            except:
                os._exit(1)
                
            print('Client at '+ip+':'+str(port)+' connected...')
            
            try:
                while len(data):
                    data = self.csocket.recv(256)
                    if data:
                        decoder.feed(data)
                        for packet in decoder.decode():
                            command = packet.strip()
                            if self.receiver:
                                self.receiver.command(command)
                            else:
                                if command == 'close':
                                    os._exit(1)
                    else:
                        self.csocket.close()
            except:
                pass
                
            print('Client at '+ip+':'+str(port)+' disconnected...')

class MenuThread(threading.Thread):

    def __init__(self, ip, port, clientsocket, userthread):
        threading.Thread.__init__(self)
        self.msgtrxinit = 33
        self.msgchannelpower = 211
        self.msgsubchannel = 209
        self.msgbegin = 208
        self.msgend = 7
        self.msgbst = 215
        self.msgmer = 216
        self.msgreflect = 213
        self.ip = ip
        self.port = port
        self.csocket = clientsocket
        self.user = userthread
        self.dorun = True
        self.debug = False
        self.echoloop = False
        self.echoloopinit = False
        self.displayecho = True
        self.trxchannel = Defaultchannel
        self.trxsubchannel = Defaultsubchannel
        self.trxpower = Defaultpower
        self.trxechoes = Defaultechoes
        self.trxechosize = Defaultechoextrasize
        self.trxreflecttime = Defaultreflecttime
        self.totalechoes = 0
        self.totalreportedechoes = 0
        self.errorlevel = 0
        self.stdcolor = Defaultstdcolor
        self.output('[+] New menu thread started for '+self.ip+':'+str(self.port))
        self.readfromfile()
    
    def output(self, message):
        try:
            print(message)
        except:
            pass
        if self.user:
            self.user.output(message)
            
    def storetofile(self):
        try:
            with open('config.json', 'r') as f:
                config = json.load(f)
            
            config['Defaultchannel'] = self.trxchannel
            config['Defaultsubchannel'] = self.trxsubchannel
            config['Defaultpower'] = self.trxpower
            config['Defaultechoes'] = self.trxechoes
            config['Defaultechoextrasize'] = self.trxechosize
            config['Defaultreflecttime'] = self.trxreflecttime
            config['Defaultstdcolor'] = self.stdcolor
            
            with open('config.json', 'w') as f:
                json.dump(config, f)
        except:
            config = {'Defaultpower': self.trxpower}
            try:
                with open('config.json', 'w') as f:
                    json.dump(config, f)
            except:
                pass
                
    def readfromfile(self):
        try:
            with open('config.json', 'r') as f:
                config = json.load(f)
            
            self.trxchannel = config['Defaultchannel']
            self.trxsubchannel = config['Defaultsubchannel']
            self.trxpower = config['Defaultpower']
            self.trxechoes = config['Defaultechoes']
            self.trxechosize = config['Defaultechoextrasize']
            self.trxreflecttime = config['Defaultreflecttime']
            self.stdcolor = config['Defaultstdcolor']
        except:
            pass
        
    def seterror(self, level):
        if level != self.errorlevel:
            if level == 0:
                if self.stdcolor:
                    sys.stdout.write('\033[0;0m')
            if level == 1:
                if self.stdcolor:
                    sys.stdout.write('\033[1;33m')
            if level == 2:
                if self.stdcolor:
                    sys.stdout.write('\033[1;31m')
                
            if self.user:
                self.user.seterror(level)
        self.errorlevel = level
        
    def close(self):
        self.dorun = False
        
    def listhelp(self):
        self.output(' 01 help - Display options')
        self.output(' 02 close - Close application')
        self.output(' 03 debugoff - Disable debug logs')
        self.output(' 04 debugon - Enable debug logs')
        self.output(' 05 listparams - Display all parameters')
        self.output(' 06 listsubch - Display how to change subcarrier frequency')
        self.output(' 07 listch - Display how to change carrier frequency')
        self.output(' 08 setpower - Set power, ex: setpower 28')
        self.output(' 09 setechoes - Set number of echoes, ex: setechoes 10')
        self.output(' 10 setechosize - Set number extra data bytes for echo, ex: setechosize 1')
        self.output(' 11 setreftime - Set reflect time in seconds, ex: setreftime 60')
        self.output(' 12 echo - Start one echo')
        self.output(' 13 echoloop - Toggle enable or disable echo loop mode')
        self.output(' 14 x - Disable echo loop mode')
        self.output(' 15 reflect - Start reflect mode')
        self.output(' 16 color - Toggle enable or disable std color')
        
    def listsubch(self):
        self.output(' 50 setsubch 1.5')
        self.output(' 51 setsubch 2.0')
        
    def listch(self):
        self.output(' 60 setch 1 (5.7975 GHz)')
        self.output(' 60 setch 2 (5.8025 GHz)')
        self.output(' 60 setch 3 (5.8075 GHz)')
        self.output(' 60 setch 4 (5.8125 GHz)')
        self.output(' 60 setch 9 (5.8500 GHz)')
        self.output(' 60 setch 13 (5.9000 GHz)')
        self.output(' 60 setch 15 (5.8250 GHz)')
        self.output(' 60 setch 16 (5.7000 GHz)')
        self.output(' 60 setch 17 (5.7250 GHz)')
        self.output(' 60 setch 18 (5.7500 GHz)')
        self.output(' 60 setch 19 (5.7750 GHz)')
        self.output(' 60 setch 20 (5.8750 GHz)')
        
    def listparams(self):
        self.output('Subcarrier freq:')
        if self.trxsubchannel == 0:
            self.output(' 1.5 MHz')
        if self.trxsubchannel == 1:
            self.output(' 2.0 MHz')
        
        self.output('Carrier freq:')
        if self.trxchannel == 1:
            self.output(' 5.7975 GHz (1)')
        if self.trxchannel == 2:
            self.output(' 5.8025 GHz (2)')
        if self.trxchannel == 3:
            self.output(' 5.8075 GHz (3)')
        if self.trxchannel == 4:
            self.output(' 5.8125 GHz (4)')
        if self.trxchannel == 9:
            self.output(' 5.8500 GHz (9)')
        if self.trxchannel == 13:
            self.output(' 5.9000 GHz (13)')
        if self.trxchannel == 15:
            self.output(' 5.8250 GHz (15)')
        if self.trxchannel == 16:
            self.output(' 5.7000 GHz (16)')
        if self.trxchannel == 17:
            self.output(' 5.7250 GHz (17)')
        if self.trxchannel == 18:
            self.output(' 5.7500 GHz (18)')
        if self.trxchannel == 19:
            self.output(' 5.7750 GHz (19)')
        if self.trxchannel == 20:
            self.output(' 5.8750 GHz (20)')
            
        self.output('Output power:')
        self.output(' ' + str(self.trxpower) + ' dBm EIRP')
        
        self.output('Echo count:')
        self.output(' ' + str(self.trxechoes))
        self.output('Echo extra data bytes:')
        self.output(' ' + str(self.trxechosize))
        
        self.output('Reflect time:')
        self.output(' ' + str(self.trxreflecttime) + ' seconds')
        
        self.storetofile()
        
    def sendpacket(self, packet):
        ret = self.csocket.send(packet)
        if ret != len(packet):
            self.seterror(2)
            self.output('error send')
        data = bytearray(packet)
        
        if self.debug:
            self.output('out: ')
            self.output(''.join(format(x, '02x') for x in data))
        
    def init(self):
        self.sendtrxinit(self.trxchannel, self.trxpower)
        time.sleep(.300)
        self.sendchannelpower(self.trxchannel, self.trxpower)
        
    def sendtrxinit(self, trxchannel, trxpower):
        packet = bytearray()
        packet.append(self.msgtrxinit)
        packet.append(0x02)
        packet.append(trxchannel)
        packet.append(trxpower)
        self.sendpacket(packet)
        
    def sendchannelpower(self, trxchannel, trxpower):
        packet = bytearray()
        packet.append(self.msgchannelpower)
        packet.append(0x03)
        packet.append(0x01)
        packet.append(trxchannel)
        packet.append(trxpower)
        self.sendpacket(packet)
        
    def sendsubchannel(self, trxsubchannel):
        packet = bytearray()
        packet.append(self.msgsubchannel)
        packet.append(0x01)
        packet.append(trxsubchannel)
        self.sendpacket(packet)
        
    def sendbegin(self):
        packet = bytearray()
        packet.append(self.msgbegin)
        packet.append(0x00)
        self.sendpacket(packet)
        
    def sendend(self):
        packet = bytearray()
        packet.append(self.msgend)
        packet.append(0x00)
        self.sendpacket(packet)
        
    def sendbst(self, trxchannel, trxpower):
        packet = bytearray()
        packet.append(self.msgbst)
        packet.append(0x03)
        packet.append(trxchannel)
        packet.append(trxpower)
        packet.append(0x02)
        self.sendpacket(packet)
        
    def sendmer(self, numecho):
        result = []
        for i in range(0, 4):
            result.append(numecho >> (i * 8) & 0xff)
        
        packet = bytearray()
        packet.append(self.msgmer)
        packet.append(0x06)
        packet.append(result[3])
        packet.append(result[2])
        packet.append(result[1])
        packet.append(result[0])
        packet.append(self.trxechosize)
        packet.append(0x00)
        self.sendbst(self.trxchannel, self.trxpower)
        self.sendpacket(packet)
        self.totalechoes += numecho
        
    def sendreflect(self, trxchannel, trxpower, reflecttime):
        result = []
        reflecttimems = reflecttime * 1000
        for i in range(0, 4):
            result.append(reflecttimems >> (i * 8) & 0xff)
        
        packet = bytearray()
        packet.append(self.msgreflect)
        packet.append(0x05)
        packet.append(trxchannel)
        packet.append(trxpower)
        packet.append(result[2])
        packet.append(result[1])
        packet.append(result[0])
        self.sendpacket(packet)
    
    def resetstats(self):
        self.totalechoes = 0;
        self.totalreportedechoes = 0;
        
    def doecho(self, delay):
        time.sleep(delay)
        self.sendbegin()
        time.sleep(.300)
        self.sendchannelpower(self.trxchannel, self.trxpower)
        time.sleep(.300)
        self.sendsubchannel(self.trxsubchannel)
        time.sleep(.300)
        self.sendbst(self.trxchannel, self.trxpower)
        time.sleep(.300)
        self.sendmer(self.trxechoes)
        
    def doreflect(self):
        self.sendbegin()
        time.sleep(.300)
        self.sendchannelpower(self.trxchannel, self.trxpower)
        time.sleep(.300)
        self.sendreflect(self.trxchannel, self.trxpower, self.trxreflecttime)
        time.sleep(.300)
        self.sendsubchannel(0)
        time.sleep(.300)
        self.sendend()
        
    def command(self, command):
        if len(command) < 1:
            return
        
        self.seterror(0)
        self.output('>' + command)
        ok = ' ok'
        
        if command == 'help' or command == '01':
            self.output('help' + ok)
            self.listhelp()
            
        if command == 'close' or command == '02':
            self.output('close' + ok)
            if self.echoloop:
                self.sendend()
                time.sleep(.300)
            os._exit(1)
            
        if command == 'debugoff' or command == '03':
            self.output('debugoff' + ok)
            self.debug = False
            
        if command == 'debugon' or command == '04':
            self.output('debugon' + ok)
            self.debug = True
        
        if command == 'listparams' or command == '05':
            self.output('listparams' + ok)
            self.listparams()
            
        if command == 'listsubch'  or command == '06':
            self.output('listsubch' + ok)
            self.listsubch()
            
        if command == 'setsubch 1.5' or command == '50':
            self.output('setsubch 1.5' + ok)
            self.trxsubchannel = 0
            self.listparams()
            
        if command == 'setsubch 2.0' or command == '51':
            self.output('setsubch 2.0' + ok)
            self.trxsubchannel = 1
            self.listparams()
        
        if command == 'listch' or command == '07':
            self.output('listch' + ok)
            self.listch()
            
        if 'setch' in command or '60' in command:
            commands = command.split(' ')
            try:
                self.trxchannel = int(commands[1])
                self.output('setch ' + str(self.trxchannel) + ok)
                self.listparams()
            except:
                self.seterror(2)
                self.output('error')
                self.listch()
            
        if 'setpower' in command or '08' in command:
            commands = command.split(' ')
            try:
                self.trxpower = int(commands[1])
                self.output('setpower ' + str(self.trxpower) + ok)
                self.listparams()
            except:
                self.seterror(2)
                self.output('error')
                
        if 'setechoes' in command or '09' in command:
            commands = command.split(' ')
            try:
                self.trxechoes = int(commands[1])
                self.output('setechoes ' + str(self.trxechoes) + ok)
                self.listparams()
            except:
                self.seterror(2)
                self.output('error')
                
        if 'setechosize' in command or '10' in command:
            commands = command.split(' ')
            try:
                self.trxechosize = int(commands[1])
                self.output('setechosize ' + str(self.trxechosize) + ok)
                self.listparams()
            except:
                self.seterror(2)
                self.output('error')
                
        if 'setreftime' in command or '11' in command:
            commands = command.split(' ')
            try:
                self.trxreflecttime = int(commands[1])
                self.output('setreftime ' + str(self.trxreflecttime) + ok)
                self.listparams()
            except:
                self.seterror(2)
                self.output('error')
            
        if command == 'chpower':
            self.sendchannelpower(self.trxchannel, self.trxpower)
            
        if command == 'subch':
            self.sendsubchannel(self.trxsubchannel)
            
        if command == 'echo'or command == '12':
            self.output('echo' + ok)
            self.resetstats()
            self.doecho(1)
            self.displayecho = True
            
        if command == 'echoloop' or command == '13':
            self.output('echoloop' + ok)
            if self.echoloop:
                self.echoloop = False
                self.displayecho = False
                time.sleep(5)
                self.sendend()
            else:
                self.echoloopinit = True
                self.echoloop = True
                self.displayecho = True
                
        if command == 'x' or command == '14':
            self.output('x' + ok)
            if self.echoloop:
                self.echoloop = False
                self.displayecho = False
                time.sleep(5)
                self.sendend()
        
        if command == 'reflect' or command == '15':
            self.output('reflect' + ok)
            self.doreflect()
            
        if command == 'color' or command == '16':
            self.output('color' + ok)
            if self.stdcolor:
                self.stdcolor = False
            else:
                self.stdcolor = True
        
    def parsepacket(self, packet):
        data = bytearray(packet)
        
        if self.debug:
            self.output('in: ')
            self.output(''.join(format(x, '02x') for x in data))
            
        if data[0] == self.msgmer:
            reportedechoes = 0
            if len(data) > 2:
                length = data[1]
                if length == 5:
                    try:
                        b = bytearray()
                        b.append(data[3])
                        b.append(data[4])
                        b.append(data[5])
                        b.append(data[6])
                        reportedechoes = struct.unpack('>L', b)[0]
                        if reportedechoes > 5000:
                            self.seterror(2)
                            self.output('error invalid value please restart trx')
                            reportedechoes = 0
                        self.totalreportedechoes += reportedechoes
                    except:
                        self.seterror(2)
                        self.output('error parse mer')
                
                        
                fer = (self.totalechoes - self.totalreportedechoes) / self.totalechoes
                ber = 1 - math.pow(1 - fer, 1 / (8 * (self.trxechosize + 10)))
                berstr = str(round(ber, 2))
                
                if self.trxechoes == reportedechoes:
                    self.seterror(0)
                else:
                    if reportedechoes == 0:
                        self.seterror(2)
                    else:
                        self.seterror(1)
                
                if self.displayecho:
                    self.output('Echo:' + str(reportedechoes) + '/' + str(self.trxechoes) + ':' + str(self.totalreportedechoes) + '/' + str(self.totalechoes) + ':' + berstr)
                
            if self.echoloop:
                if reportedechoes == 0:
                    self.doecho(5)
                else:
                    self.sendmer(self.trxechoes)
                    
        if data[0] == self.msgreflect:
            if len(data) > 2:
                if data[2] == 0x00:
                    self.seterror(0)
                    self.output('reflect rsp ok')
                else:
                    self.seterror(2)
                    self.output('reflect rsp error')
                
    def run(self):
        time.sleep(1)
        self.output(' ')
        self.output('help')
        self.listhelp()
        self.output(' ')
        self.output('listparams')
        self.listparams()
        
        while self.dorun:
            time.sleep(1)
            if self.echoloop:
                if self.echoloopinit:
                    self.echoloopinit = False
                    self.resetstats()
                    self.doecho(1)
           
class PacketDecoder(object):

    def __init__(self):
        self._stream = ''

    def feed(self, buf):
        self._stream += buf

    def decode(self):
        '''
        Yields packets from the current stream.
        ''' 
        while len(self._stream) > 2:
            packet_len = ord(self._stream[1])
            if len(self._stream) < packet_len + 1:
                break

            end = packet_len + 1 + 1
            packet = self._stream[:end]
            yield packet
            self._stream = self._stream[end:]

class ClientThread(threading.Thread):

    def __init__(self, ip, port, clientsocket, menuthread):
        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.csocket = clientsocket
        self.menuthread = menuthread
        self.menuthread.output('[+] New comm thread started for '+self.ip+':'+str(self.port))

    def run(self):
        data = ' '
        
        decoder = PacketDecoder()
        
        self.menuthread.init()
        
        try:
            while len(data):
                data = self.csocket.recv(256)
                if data:
                    decoder.feed(data)
                    for packet in decoder.decode():
                        self.menuthread.parsepacket(packet)
                else:
                    self.csocket.close()
        except:
            pass
            
        self.menuthread.output('Client at '+self.ip+':'+str(self.port)+' disconnected...')
        self.menuthread.close()

def main():
    print('Echolotpy by www.kapsch.net ver: 0.4')
    standalone = False
    if len(sys.argv) > 1:
        standalone = True
    
    localip = Configlocalip
    localport = Configlocalport
    localuserip = Configlocaluserip
    localuserport = Configlocaluserport

    tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    tcpsock.bind((localip, int(localport)))
    
    if standalone == False:
        newstdthread = UserStdThread()
        newstdthread.start()
    
    newuserthread = UserSocketThread(localuserip, localuserport)
    newuserthread.start()

    while True:
        tcpsock.listen(0)
        try:
            (clientsock, (ip, port)) = tcpsock.accept()
        except:
            os._exit(1)
        
        newmenuthread = MenuThread(ip, port, clientsock, newuserthread)
        newmenuthread.start()
        
        if standalone == False:
            newstdthread.setreceiver(newmenuthread)
        newuserthread.setreceiver(newmenuthread)
        
        newcommthread = ClientThread(ip, port, clientsock, newmenuthread)
        newcommthread.start()
        # wait for comm thread to exit
        newcommthread.join()
        # wait for menu thread to exit
        newmenuthread.join()
        # disable pass data
        if standalone == False:
            newstdthread.setreceiver(None)
        newuserthread.setreceiver(None)
    
if __name__ == '__main__':
    main()
